// async function that fetches a user by username from github api
export const fetchUser = async (username: string): Promise<any> => {
  try {
    const res = await fetch(`https://api.github.com/users/${username}`);
    const user = await res.json();
    return user;
  } catch (error) {
    console.log(error);
    return null;
  }
};
