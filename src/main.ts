import "./style.css";
import { concatMap, debounceTime, fromEvent, map, Observable, of } from "rxjs";
import { fetchUser } from "./util/fetch";

const input = document.getElementById("input") as HTMLInputElement;
console.log(input);

let user$: Observable<any>;

const text$: Observable<string> = fromEvent(input, "input").pipe(
  debounceTime(1000),
  map((e: any) => e.target.value)
);

// nested subscribing
text$.subscribe(async (text) => {
  const user = text && (await fetchUser(text));
  user$ = of(user);

  user$.subscribe((user: any) => {
    console.log("nested subscribing", user);
  });
});

// concatMap approach
text$
  .pipe(concatMap((text: string) => fetchUser(text).then((user) => user)))
  .subscribe((user: any) => {
    console.log("concatMap", user);
  });
